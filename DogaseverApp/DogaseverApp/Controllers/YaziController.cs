﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DogaseverApp.Models;

namespace DogaseverApp.Controllers
{
    public class YaziController : Controller
    {
        private DogaseverDBContext db = new DogaseverDBContext();

        // GET: Yazi
        public ActionResult Index(string searchString)
        {
            var yazilar = from m in db.Yazilar
                         select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                yazilar = yazilar.Where(s => s.Baslik.Contains(searchString));
            }

            return View(yazilar);

            //return View(db.Yazilar.ToList());
        }

        // GET: Yazi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazi yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }

        // GET: Yazi/Create
        public ActionResult Create()
        {
           
            return View();
        }

        // POST: Yazi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "YaziID,Baslik,Icerik,EklenmeTarihi")] Yazi yazi)
        {
            if (ModelState.IsValid)
            {          
                db.Yazilar.Add(yazi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(yazi);
        }

        // GET: Yazi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazi yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }

        // POST: Yazi/Edit/5        
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "YaziID,Baslik,Icerik,EklenmeTarihi")] Yazi yazi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yazi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yazi);
        }

        // GET: Yazi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Yazi yazi = db.Yazilar.Find(id);
            if (yazi == null)
            {
                return HttpNotFound();
            }
            return View(yazi);
        }

        // POST: Yazi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Yazi yazi = db.Yazilar.Find(id);
            db.Yazilar.Remove(yazi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
