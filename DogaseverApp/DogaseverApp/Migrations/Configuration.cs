namespace DogaseverApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DogaseverApp.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<DogaseverApp.Models.DogaseverDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DogaseverApp.Models.DogaseverDBContext context)
        {
            context.Yazilar.AddOrUpdate(i => i.Baslik,
       new Yazi
       {
           Baslik = "Doga Sevgisi",
           Icerik = "Doga sevgisiyle alakali icerik",
           EklenmeTarihi = DateTime.Parse("2016-10-11"),                    
       },

     new Yazi
     {
         Baslik = "Cadir Secimi",
         Icerik = "Cadir secimiyle alakali icerik",
         EklenmeTarihi = DateTime.Parse("2016-10-9"),
     }, new Yazi
     {
         Baslik = "Kamp Yerleri",
         Icerik = "Turkiyedeki kamp yerleriyle alakali icerik",
         EklenmeTarihi = DateTime.Parse("2016-10-7"),
     }, new Yazi
     {
         Baslik = "Dogada Yon Bulma",
         Icerik = "Dogada yon bulmayla alakali icerik",
         EklenmeTarihi = DateTime.Parse("2016-10-6"),
     }, new Yazi
     {
         Baslik = "Kamp Yeri Secimi",
         Icerik = "Kamp yeri secimiyle alakali icerik",
         EklenmeTarihi = DateTime.Parse("2016-10-4"),
     }
  );
        }
    }
}
