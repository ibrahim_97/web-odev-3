namespace DogaseverApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Yazis",
                c => new
                    {
                        YaziID = c.Int(nullable: false, identity: true),
                        Baslik = c.String(),
                        Icerik = c.String(),
                        EklenmeTarihi = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.YaziID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Yazis");
        }
    }
}
