namespace DogaseverApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAnnotation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Yazis", "Baslik", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("dbo.Yazis", "Icerik", c => c.String(nullable: false, maxLength: 600));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Yazis", "Icerik", c => c.String(maxLength: 600));
            AlterColumn("dbo.Yazis", "Baslik", c => c.String(maxLength: 60));
        }
    }
}
