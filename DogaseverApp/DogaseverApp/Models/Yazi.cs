﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace DogaseverApp.Models
{
    public class Yazi
    {
        public int YaziID { get; set; }

        [Required]
        [Display(Name = "Başlık")]
        [StringLength(60, MinimumLength = 3)]
        public string Baslik { get; set; }

        [Required]
        [Display(Name = "İçerik")]
        [StringLength(600, MinimumLength = 3)]
        public string Icerik { get; set; }

        [Display(Name = "Eklenme Tarihi")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]     
        public DateTime EklenmeTarihi { get; set; }
    }

    public class DogaseverDBContext : DbContext
    {
        public DbSet<Yazi> Yazilar { get; set; }
    }

}