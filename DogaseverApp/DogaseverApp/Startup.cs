﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DogaseverApp.Startup))]
namespace DogaseverApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
